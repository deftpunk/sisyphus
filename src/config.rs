use std::collections::HashMap;
use std::fs;
use std::path::PathBuf;

use url::Url;

use crate::work;

use tracing::{trace, debug, info, warn, error};

#[derive(Default, Debug)]
pub struct GlobalConf {
    pub environment: Option<HashMap<String, String>>,
    pub interpreter: Option<PathBuf>,
    pub jobs_dir: Option<PathBuf>,
}

#[derive(Default, Debug)]
pub struct JobConf {
    pub global: GlobalConf,
    pub jobs: HashMap<String, work::JobType>,
}

impl JobConf {
    fn load(config_path: &str) -> anyhow::Result<JobConf> {
        let job_config_path: PathBuf = PathBuf::from(config_path);
        let contents: String = fs::read_to_string(job_config_path)?;
        let conf = toml::from_str::<toml::Table>(&contents)?;
        let mut job_conf_map: HashMap<String, work::JobType> = HashMap::new();
        let mut global: GlobalConf = GlobalConf::default();

        // The global conf is picked out in the first pass. This ensures that
        // its value is available when processing job configs, regardless of
        // how how the "jobs" and "global" configs are ordered.
        for (conf_key, conf_value) in &conf {
            if conf_key == "global" {
                global = GlobalConf {
                    environment: getmap(&conf_value, "environment", None),
                    interpreter: getpath(&conf_value, "interpreter", None),
                    jobs_dir: getpath(&conf_value, "jobs_dir", None),
                };

                break
            }
        }

        for (conf_key, conf_value) in &conf {
            if conf_key == "jobs" {
                if let toml::Value::Table(jobs) = conf_value.clone() {
                    for (name, job_conf) in jobs {
                        let j = work::JobType {
                            name: name.to_string(),
                            path: getpath(&job_conf, "path", None),
                            extension: getstring(&job_conf, "extension", None),
                            is_realtime: getbool(&job_conf, "is_realtime", false),
                            is_sticky: getbool(&job_conf, "is_sticky", false),
                            is_concurrent: getbool(&job_conf, "is_concurrent", false),
                            is_self_cleaning: getbool(&job_conf, "is_self_cleaning", false),
                            is_batchable: getbool(&job_conf, "is_batchable", false),
                            environment: getmap(&job_conf, "environment", global.environment.clone()),
                            interpreter: getpath(&job_conf, "interpreter", global.interpreter.clone()),
                        };

                        job_conf_map.insert(name, j);
                    }
                }
            }
        }

        Ok(JobConf {
            global,
            jobs: job_conf_map,
        })
    }
}

#[derive(Debug)]
pub struct WorkerConf {
    pub worker_name: String,
    pub worker_threads: u16,
    pub connection: Option<Url>,
    pub log_level: Option<String>,
    pub job_conf: Option<JobConf>
}

impl WorkerConf {
    pub fn load(config_path: &str) -> anyhow::Result<Self> {
        let worker_config_path: PathBuf = PathBuf::from(config_path);
        let contents: String = fs::read_to_string(worker_config_path)?;
        let conf = toml::from_str::<toml::Table>(&contents)?;
        let mut worker_conf: WorkerConf = WorkerConf::default();

        for entry in conf {
            let (key, value) = entry;
            match key.as_str() {
                "worker_name" => worker_conf.worker_name = value.try_into()?,
                "worker_threads" => worker_conf.worker_threads = value.try_into()?,
                "connection" => {
                    let str_val: String = value.try_into()?;
                    worker_conf.connection = Some(Url::parse(&str_val)?)
                },
                "log_level" =>  worker_conf.log_level = value.try_into()?,
                "job_conf" => {
                    let job_conf_path: PathBuf = value.try_into()?;
                    let job_conf_path_str = job_conf_path.as_os_str().to_str().ok_or(anyhow::anyhow!("Failed to load job config"))?;
                    dbg!(&job_conf_path_str);
                    worker_conf.job_conf = match JobConf::load(job_conf_path_str) {
                        Ok(s) => Ok(Some(s)),
                        Err(e) => {
                            Err(anyhow::anyhow!("Failed to load job config: {}", e))
                        }
                    }?;
                }
                _ => {
                    // Unknown option. Ignore.
                    continue
                }
            };
        };

        Ok(worker_conf)
    }
}

impl Default for WorkerConf {
    fn default() -> Self {
        Self {
            worker_name: "worker".to_string(),
            worker_threads: 2,
            connection: None,
            log_level: None,
            job_conf: None,
        }
    }
}

fn getstring(value: &toml::Value, name: &str, default: Option<String>) -> Option<String> {
    match value.get(name) {
        Some(v) => {
            if let Some(s) = v.as_str() {
                Some(s.to_string())
            } else {
                default
            }
        },
        None => default,
    }
}

fn getbool(value: &toml::Value, name: &str, default: bool) -> bool {
    match value.get(name) {
        Some(v) => v.as_bool().unwrap_or(default),
        None => default,
    }
}

fn getmap(
    value: &toml::Value,
    name: &str,
    default: Option<HashMap<String, String>>,
) -> Option<HashMap<String, String>> {
    match value.get(name) {
        Some(v) => {
            if let Some(t) = v.as_table() {
                let mut m: HashMap<String, String> = HashMap::new();
                for (k, val) in t {
                    if let Some(s) = val.as_str() {
                        m.insert(k.clone(), s.to_string());
                    }
                }
                Some(m)
            } else {
                default
            }
        }
        None => default,
    }
}

fn getpath(value: &toml::Value, name: &str, default: Option<PathBuf>) -> Option<PathBuf> {
    match value.get(name) {
        Some(v) => {
            if let Some(s) = v.as_str() {
                Some(PathBuf::from(s))
            } else {
                default
            }
        }
        None => default,
    }
}

pub fn load_job_types(config_path: &str) -> anyhow::Result<HashMap<String, work::JobType>> {
    let conf = JobConf::load(config_path)?;

    Ok(conf.jobs)
}
