use std::cmp::Ordering;
use std::collections::HashMap;
use std::path::PathBuf;

use chrono::NaiveDateTime;

use sqlx::postgres;
use sqlx::{FromRow, Row};

use uuid::Uuid;

#[derive(Debug)]
pub enum Parameters {
    Json(String),
    List(String),
}

#[derive(Default, Debug, Clone)]
pub struct JobType {
    pub name: String,
    pub path: Option<PathBuf>,
    pub extension: Option<String>,
    pub is_realtime: bool,
    pub is_concurrent: bool,
    pub is_sticky: bool,
    pub is_self_cleaning: bool,
    pub is_batchable: bool,
    pub environment: Option<HashMap<String, String>>,
    pub interpreter: Option<PathBuf>,
}

impl JobType {
    fn new(name: String) -> Self {
        Self {
            name,
            ..Default::default()
        }
    }
}

impl PartialEq for JobType {
    fn eq(&self, other: &Self) -> bool {
        if self.name == other.name {
            true
        } else {
            false
        }
    }
}

impl PartialOrd for JobType {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        // Realtime jobs are always higher priority
        if self.is_realtime {
            if !other.is_realtime {
                Some(Ordering::Less)
            } else {
                Some(Ordering::Greater)
            }
        } else if other.is_realtime {
            Some(Ordering::Greater)
        } else {
            Some(Ordering::Equal)
        }
    }
}

#[derive(Default, Debug)]
pub struct Job {
    pub id: i64,
    pub job_type: JobType,
    pub parameters: Option<Parameters>,
    pub runtime: NaiveDateTime,
    pub priority: i32,
    pub key: u64,
}

impl Job {
    pub fn new(name: String, job_type: JobType, parameters: Option<Parameters>) -> Self {
        Self {
            job_type: job_type,
            parameters,
            ..Default::default()
        }
    }
}

impl FromRow<'_, postgres::PgRow> for Job {
    fn from_row(row: &postgres::PgRow) -> sqlx::Result<Self> {
        Ok(Self {
            id: row.try_get("job_id")?,
            job_type: JobType::new(row.try_get("module")?),
            // We don't actually care about the parameters. They are just
            // necessary to dedup jobs.
            parameters: None,
            // TODO: Handle parse failure
            runtime: row.try_get("runtime")?,
            priority: row.try_get("priority")?,
            key: {
                // Hash the job parameters so identical parameters can be
                // found efficiently.
                let key: String = row.try_get("parameters")?;
                seahash::hash(key.as_bytes())
            },
        })
    }
}

impl Eq for Job {}

impl PartialEq for Job {
    fn eq(&self, other: &Self) -> bool {
        if self.id == other.id {
            true
        } else {
            false
        }
    }
}

impl Ord for Job {
    fn cmp(&self, other: &Self) -> Ordering {
        // Realtime jobs are always highest priority
        if self.job_type.is_realtime {
            if !other.job_type.is_realtime {
                return Ordering::Less;
            } else {
                // Both are realtime. Compare other attributes below.
            }
        } else if other.job_type.is_realtime {
            return Ordering::Greater;
        }

        if self.runtime == other.runtime {
            if self.priority == other.priority {
                Ordering::Equal
                // Lower priority values indicate *greater* priority.
            } else if self.priority < other.priority {
                Ordering::Less
            } else {
                Ordering::Greater
            }
        // Newer jobs have less priority than older jobs
        } else if self.runtime > other.runtime {
            Ordering::Greater
        } else if self.runtime == other.runtime {
            Ordering::Equal
        } else {
            Ordering::Less
        }
    }
}

impl PartialOrd for Job {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

#[derive(Debug, Clone)]
pub enum BatchStatus {
    Pending,
    Complete,
    Failed,
}

#[derive(Debug, Clone)]
pub struct Batch {
    pub id: Uuid,
    pub job_type: JobType,
    pub size: i32,
    pub status: BatchStatus,
}

impl Batch {
    pub fn new(job_type: JobType, size: i32) -> Self {
        Self {
            id: Uuid::new_v4(),
            job_type: job_type,
            size,
            status: BatchStatus::Pending,
        }
    }

    fn execute(&self) {
        dbg!(self);
    }
}
