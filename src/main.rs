use std::collections::{BinaryHeap, HashMap, VecDeque};
use std::io;
use std::os::unix::process::ExitStatusExt;
use std::path::PathBuf;
use std::process::{Command, ExitStatus};
use std::sync::RwLock;
use std::thread;
use std::time::Duration;

use anyhow;

use crossbeam::{
    channel,
    deque::{Injector, Steal},
};

use chrono::{
    offset::Local,
};

use tokio::signal::unix::{signal, SignalKind};

use tracing::{trace, debug, info, warn, error};
use tracing_subscriber::{
    FmtSubscriber,
    filter::LevelFilter
};

mod config;
mod storage;
mod work;

static JOB_BASE_DIR: &str = "jobs/";
static JOB_CONFIG_FILENAME: &str = "job_config.toml";

#[derive(Default, Debug)]
struct Work {
    jobs: BinaryHeap<work::Job>,
    batches: VecDeque<work::Batch>,
}

impl Work {
    async fn fetch(&mut self, job_store: &mut storage::JobStore, worker_id: i32, job_types: &HashMap<String, work::JobType>) -> Option<usize> {
        let mut new_jobs: BinaryHeap<work::Job> = {
            let jobs = match job_store.fetch_work(worker_id, job_types).await {
                Ok(j) => j,
                Err(e) => {
                    error!("Could not fetch work: {:?}", e);
                    Vec::new()
                }
            };
            BinaryHeap::from(jobs)
        };
        let new_jobs_count = new_jobs.len();
        if new_jobs_count > 0 {
            self.jobs.append(&mut new_jobs);
            Some(new_jobs_count)
        } else {
            None
        }
    }

    async fn assign_batch(
        &mut self,
        job_store: &mut storage::JobStore,
        worker_id: i32,
    ) -> Option<work::Batch> {
        if let Some(job) = self.next_job() {
            match job_store.batch_work(job, worker_id).await {
                Ok(batch) => {
                    trace!("Adding batch");
                    self.batches.push_back(batch.clone());
                    Some(batch)
                },
                Err(err) => {
                    error!("Could not assign batch: {}", err);
                    None
                }
            }
        } else {
            trace!("No job available");
            None
        }
    }

    fn next_job(&mut self) -> Option<work::Job> {
        let mut set_aside: BinaryHeap<work::Job> = BinaryHeap::new();
        // Try a finite number of times to find a new kind of job.
        let max_tries = 16;
        let tries = 0;
        let tries_exceeded = false;
        loop {
            if let Some(peek_job) = self.jobs.peek() {
                // Always pick realtime jobs. Don't dilly-dally.
                if peek_job.job_type.is_realtime {
                    break;
                }

                // Compare the upcoming job type to those that are currently
                // batched.
                let is_new_type = {
                    let mut new_type = true;
                    for b in self.batches.iter() {
                        if b.job_type == peek_job.job_type {
                            new_type = false;
                            break;
                        }
                    }

                    new_type
                };

                // If this is a new kind of job, then our work is done.
                if is_new_type {
                    break;
                }

                // Keep looking for a different kind of job.
                if let Some(j) = self.jobs.pop() {
                    set_aside.push(j)
                }
            } else {
                // No jobs
                break;
            }

            // We've made an effort, but this is taking too long.
            if tries > max_tries {
                break;
            }
        }

        let selected_job = if tries_exceeded {
            set_aside.pop()
        } else {
            self.jobs.pop()
        };

        // Put the rest back on the heap.
        self.jobs.append(&mut set_aside);

        selected_job
    }

    fn next_batch(&mut self) -> Option<work::Batch> {
        match self.batches.pop_front() {
            Some(b) => {
                trace!("Got batch");
                Some(b)
            },
            None => {
                trace!("No batch");
                None
            }
        }
    }

    async fn cleanup(&mut self, job_store: &mut storage::JobStore, batch: work::Batch) {
        match batch.status {
            work::BatchStatus::Complete => {
                debug!("Cleaning up");
                // TODO: Handle Error
                let result = job_store.cleanup_batch(batch, true).await;
                dbg!(result);
            }
            work::BatchStatus::Failed => {
                error!("Job failed.");
                let result = job_store.cleanup_batch(batch, false).await;
                dbg!(result);
            },
            work::BatchStatus::Pending => error!("Job pending. nothing to do."),
        }
    }
}

struct Executor {
    interpreter: Option<PathBuf>,
    env: Option<PathBuf>,
    base_dir: Option<PathBuf>,
    environment: Option<HashMap<String, String>>,
}

impl Default for Executor {
    fn default() -> Self {
        Self {
            interpreter: None,
            env: None,
            base_dir: Some(PathBuf::from(JOB_BASE_DIR)),
            environment: None,
        }
    }
}

impl Executor {
    fn run(self, batch: &work::Batch) -> io::Result<ExitStatus> {
        let job_type = &batch.job_type;
        trace!("Executing batch {}", &batch.id);

        /* XXX: The interpreter, environment, and command path are not going
         * to change. So this work really should not be happening on each
         * execution.
         */

        // Use path specified on the job config, if given.
        let mut job_path = if let Some(pth) = &job_type.path {
            if pth.is_absolute() {
                (*pth).clone()
            } else {
                if let Some(base) = self.base_dir {
                    base.join(pth)
                } else {
                    // XXX: hope this is OK. If not, it may fail on exec.
                    // The best thing to do here is probably to raise an error,
                    // because this could be the cause of some surprising
                    // behavior, varying with the CWD of the executing process.
                    warn!("Relative path for job: {}", pth.to_string_lossy());
                    (*pth).clone()
                }
            }
        } else if let Some(base) = self.base_dir {
            base.join(&job_type.name)
        } else {
            warn!("Relative path for job: {}", &job_type.name);
            PathBuf::from(&job_type.name)
        };

        dbg!(&job_type);
        if let Some(ext) = &job_type.extension {
            job_path.set_extension(ext);
        }

        let mut cmd_args: Vec<String> = Vec::new();

        // Check for a job specific interpreter, and use if present.
        let cmd_path = if let Some(interp) = &job_type.interpreter {
            cmd_args.push(job_path.display().to_string());
            PathBuf::from(interp)
        // Otherwise use the default interpreter, if configured.
        } else if let Some(interp) = self.interpreter {
            cmd_args.push(job_path.display().to_string());
            interp
        // If no interpreter is provided, execute the job directly.
        } else {
            job_path
        };

        // Use a job specific environment, if available
        let empty_env: HashMap<String, String> = HashMap::new();
        let environment = if let Some(env) = &job_type.environment {
            env
        // Otherwise, use the default environment
        } else if let Some(env) = &self.environment {
            env
        // If all else fails, make an empty one.
        } else {
            &empty_env
        };

        let uuid_str = format!("{}", &batch.id.as_simple());
        cmd_args.push(uuid_str);

        let mut cmd = Command::new(cmd_path);
        let cmd_w_args = cmd.args(cmd_args).envs(environment);

        let status = cmd_w_args.status();

        dbg!(&cmd_w_args);
        dbg!(&status);

        status
    }
}

#[derive(Default)]
struct Manager {
    work: Work,
    job_types: HashMap<String, work::JobType>,
}

impl Manager {
    fn init(&mut self, conf: &config::WorkerConf) -> anyhow::Result<()> {
        // TODO: Fix error handling
        self.job_types = conf.job_conf.as_ref().unwrap().jobs.clone();

        Ok(())
    }

    fn select_work(&mut self) -> Option<work::Batch> {
        trace!("Selecting work");
        self.work.next_batch()
    }

    // XXX: Error handling
    fn delegate_work(&mut self, queue: &Injector<work::Batch>) -> Result<(), ()> {
        if let Some(batch) = self.select_work() {
            trace!("Queueing batch");
            queue.push(batch);
        }
        Ok(())
    }
}

async fn run_service(conf: &config::WorkerConf) -> anyhow::Result<()> {
    let mut sigint = signal(SignalKind::interrupt())?;

    let worker_name: &str = conf.worker_name.as_str();
    let worker_threads: u16 = conf.worker_threads;
    let connection_string: &str = conf.connection.as_ref().unwrap().as_str();

    let mut job_store = storage::JobStore::new(connection_string).await;
    let worker_id = job_store.initialize(Some(worker_name)).await?;

    let mut mgr = Manager::default();
    // TODO: No error handling
    mgr.init(conf)?;

    // This is the queue our worker threads will pull work from.
    let work_queue: Injector<work::Batch> = Injector::new();

    // No work stealing, yet.
    //let s = work_queue.stealer();
    let (cleanup_tx, cleanup_rx) = channel::unbounded::<work::Batch>();

    // Mark job store as mutable
    let mut job_store = job_store;

    let mut stop_running = RwLock::new(false);

    thread::scope(|s| {
        info!("Signal handler thread.");
        let _signal_handler = s.spawn(|| {
            // If it ever stops blocking, set the flag.
            futures::executor::block_on(sigint.recv());
            trace!("Got SIGINT. Setting quit flag.");
            let mut flag = stop_running.write().unwrap();
            *flag = true;
        });

        info!("Starting Manager thread.");
        let injector_handle = s.spawn(|| {
            // We're off the main thread, where the tokio runtime is handling execution of futures.
            // So, any async functions must be forced to run synchronously, using
            // `futures::executor::block_on()`.

            let mut reg_init = false;
            let mut last_check = (Local::now() - chrono::Duration::seconds(60)).naive_local();
            let mut expiration = (Local::now() - chrono::Duration::seconds(60)).naive_local();
            let rereg_window = chrono::Duration::seconds(65);
            let min_check_interval = chrono::Duration::seconds(5);

            loop {
                trace!("Manager thread: Handling cleanup");
                loop {
                    trace!("Manager thread: Waiting on cleanup messages");
                    if let Ok(batch) = cleanup_rx.try_recv() {
                        trace!("Manager thread: Cleanup batch");
                        futures::executor::block_on(mgr.work.cleanup(&mut job_store, batch));
                    } else {
                        trace!("Manager thread: Nothing to clean up.");
                        break;
                    }
                }

                // Stop *after* cleanup is done.
                if *stop_running.read().unwrap() {
                    trace!("Manager Thread: Quitting.");
                    break
                }

                let now = Local::now().naive_local();
                let time_til_expiration = expiration - now;
                // Do not assume registration has expired until *after* first registration.
                if time_til_expiration <= chrono::Duration::seconds(0) && reg_init {
                    error!("Manager thread: Registration expired.");
                    continue
                // Attempt to re-register `rereg_window` (~1 minute) before registration expires,
                // and do not attempt to register more frequently than every `min_check_interval`
                // (5) seconds.
                } else if time_til_expiration <= rereg_window  && (now - last_check) > min_check_interval {
                    match futures::executor::block_on(job_store.keep_alive(worker_id)) {
                        Ok((_i, t)) => {
                            expiration = t;
                            last_check = now;
                            reg_init = true;
                        }
                        Err(e) => {
                            error!("Manager thread: Failed to update registration: {}", e);
                        }
                    }
                }
                // TODO: Error handling (Result values)
                match futures::executor::block_on(job_store.request_work(worker_id)) {
                    Ok(_) => {
                        trace!("Manager thread: Work request succeeded.");
                    },
                    Err(e) => {
                        trace!("Manager thread: Work request failed: {}", e);
                    }
                };

                if futures::executor::block_on(mgr.work.fetch(&mut job_store, worker_id, &conf.job_conf.as_ref().unwrap().jobs)).is_none()
                {
                    trace!("Manager thread: Sleeping");
                    // No work available. Wait a bit.
                    thread::sleep(Duration::from_millis(1000));
                };

                futures::executor::block_on(mgr.work.assign_batch(&mut job_store, worker_id));
                trace!("Manager thread: Delegating work to worker threads");
                // TODO: Error Handling
                mgr.delegate_work(&work_queue);
            }
        });

        let mut worker_handles: Vec<thread::ScopedJoinHandle<()>> = Vec::new();

        for thread_id in 0..worker_threads {
            info!("Starting worker thread {}", thread_id);
            // Allow copies of the references to these items to be moved into each thread.
            let work_queue = &work_queue;
            let cleanup_tx = &cleanup_tx;
            let stop_running = &stop_running;
            let exec_env = if let Some(ref c) = &conf.job_conf {
                c.global.environment.clone()
            } else {
                None
            };
            let exec_base_dir = if let Some(ref c) = &conf.job_conf {
                c.global.jobs_dir.clone()
            } else {
                None
            };
            let exec_interpreter = if let Some(ref c) = &conf.job_conf {
                c.global.interpreter.clone()
            } else {
                None
            };


            worker_handles.push(s.spawn(move || loop {
                if *stop_running.read().unwrap() {
                    trace!("Worker Thread {}: Quitting.", thread_id);
                    break
                }

                trace!("Starting work execution");
                // Pull work from the work queue
                if let Steal::Success(mut batch) = work_queue.steal() {
                    trace!("Stole work");
                    // Execute the batch
                    let mut exec = Executor::default();
                    exec.environment = exec_env.clone();
                    exec.interpreter = exec_interpreter.clone();
                    exec.base_dir = exec_base_dir.clone();
                    match exec.run(&batch) {
                        Ok(status) => {
                            if let Some(_exitcode) = status.code() {
                                if status.success() {
                                    trace!("Worker Thread {}: Batch {} exited successfully", thread_id, &batch.id);
                                    // TODO: Job completed successfully.
                                    batch.status = work::BatchStatus::Complete;
                                } else {
                                    trace!("Worker Thread {}: Batch {} exited with error", thread_id, &batch.id);
                                    // TODO: Job failed. Do something appropriate
                                    batch.status = work::BatchStatus::Failed
                                }
                            } else if let Some(_sigcode) = status.signal() {
                                trace!("Worker Thread {}: Batch {} killed with signal", thread_id, &batch.id);
                                // TODO: This is not success. Do some modified failure
                                // handling.
                                batch.status = work::BatchStatus::Failed
                            }
                        }
                        Err(_e) => {
                            trace!("Worker Thread {}: Batch {} failed to execute", thread_id, &batch.id);
                            // TODO: Job failed to even execute.
                            batch.status = work::BatchStatus::Failed
                        }
                    }

                    // TODO: Handle failure to send cleanup message.
                    let _ = cleanup_tx.send(batch.clone());
                } else {
                    trace!("Worker Thread {}: Sleeping", thread_id);
                    // No work available. Wait a bit.
                    thread::sleep(Duration::from_millis(1000));
                }
            }));
        }

        trace!("Waiting on worker threads.");
        // Wait for everything to finish.
        for wrkr in worker_handles {
            wrkr.join().unwrap();
        }
        trace!("Worker threads finished.");
        injector_handle.join().unwrap();
        trace!("Injector thread finished.");
    });
    Ok(())
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let conf = config::WorkerConf::load("worker.toml")?;

    let log_level = if let Some(l) = conf.log_level.as_ref() {
        match l.as_str() {
            "trace" => LevelFilter::TRACE,
            "debug" => LevelFilter::DEBUG,
            "info" => LevelFilter::INFO,
            "warn" => LevelFilter::WARN,
            "error" => LevelFilter::ERROR,
            "off" => LevelFilter::OFF,
            _ => anyhow::bail!("Invalid log level: {}", l),
        }
    } else {
        LevelFilter::INFO
    };

    let subscriber = FmtSubscriber::builder()
        .with_max_level(log_level.clone())
        .finish();
    tracing::subscriber::set_global_default(subscriber).expect("Failed to set tracing defaults.");
    info!("Log level set to {}", log_level);

    run_service(&conf).await
}
