use std::collections::HashMap;

use futures::stream::TryStreamExt;

use sqlx::postgres::PgConnection;
use sqlx::{Connection, FromRow, Row};

use chrono::NaiveDateTime;
use crate::work::{Batch, Job, JobType};

use tracing::trace;

use uuid::Uuid;

pub struct JobStore {
    conn: PgConnection,
}

impl JobStore {
    pub async fn new(connection_string: &str) -> Self {
        let conn = PgConnection::connect(connection_string).await.unwrap();

        Self { conn }
    }

    pub async fn init_test_schema(&mut self) -> Result<(), sqlx::Error> {
        sqlx::query(
            "
                BEGIN;
                    CREATE TABLE async_worker (
                        id SERIAL PRIMARY KEY,
                        name TEXT NOT NULL,
                        registration_start TIMESTAMP WITHOUT TIME ZONE NOT NULL,
                        registration_end TIMESTAMP WITHOUT TIME ZONE NOT NULL,
                        job_types TEXT[]
                    );
                    CREATE TABLE async_job_assignment (
                        id SERIAL PRIMARY KEY,
                        job_id INTEGER UNIQUE NOT NULL,
                        worker_id INTEGER NOT NULL,
                        loaded BOOLEAN,
                        batch_id UUID
                    );
                    CREATE TABLE scheduler_job_que (
                        id SERIAL PRIMARY KEY,
                        module TEXT,
                        entrytime TIMESTAMP,
                        runtime TIMESTAMP,
                        parameters TEXT,
                        priority INTEGER DEFAULT 2
                    );
                    CREATE TABLE scheduler_results (
                        id SERIAL PRIMARY KEY,
                        scheduler_job_id SERIAL PRIMARY KEY,
                        status TEXT,
                        completed TIMESTAMP WITH TIME ZONE,
                        percent_done INTEGER,
                        result_raw TEXT,
                        result_encoding TEXT,
                        expires TIMESTAMP WITH TIME ZONE,
                        user_id INTEGER,
                        auth_user_id INTEGER
                    );

                COMMIT;
                ",
        )
        .execute(&mut self.conn)
        .await?;

        Ok(())
    }

    pub async fn init_test_data(&mut self) -> Result<(), sqlx::Error> {
        let mut transaction = self.conn.begin().await?;
        let init_jobs = [
            ("Echo", "message1"),
            ("Echo", "message2"),
            ("Blam", "bamzagg"),
            ("Blam", "fofiels"),
            ("Echo", "message3"),
        ];

        for j in init_jobs.iter() {
            let (name, args) = j;
            sqlx::query(
                "
                    INSERT INTO scheduler_job_que (module, entrytime, runtime, parameters)
                    VALUES ($1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, $2);
                    ",
            )
            .bind(&name)
            .bind(&args)
            .execute(&mut *transaction)
            .await?;
        }

        transaction.commit().await?;
        Ok(())
    }

    pub async fn initialize(&mut self, name: Option<&str>) -> Result<i32, sqlx::Error> {
        let mut transaction = self.conn.begin().await?;
        let worker_name = match name {
            Some(n) => n.to_string(),
            None => format!("{}", Uuid::new_v4().as_simple()),
        };

        let result = sqlx::query(
            r#"
                INSERT INTO async_worker (name, registration_start, registration_end)
                VALUES($1, CURRENT_TIMESTAMP, CURRENT_TIMESTAMP + '5 min'::interval)
                RETURNING id
                ;
            "#,
        )
        .bind(&worker_name)
        .fetch_one(&mut *transaction)
        .await?;

        let worker_id: i32 = result.try_get("id")?;

        transaction.commit().await?;

        Ok(worker_id)
    }

    pub async fn keep_alive(&mut self, worker_id: i32) -> Result<(i32, NaiveDateTime), sqlx::Error> {
        let mut transaction = self.conn.begin().await?;

        let result = sqlx::query(
            r#"
                UPDATE async_worker
                SET registration_end = CURRENT_TIMESTAMP + '5 min'::interval
                WHERE (
                    id = $1
                    AND registration_end < CURRENT_TIMESTAMP - '1 min'::interval
                )
                RETURNING id, registration_end
                ;
            "#,
        )
        .bind(&worker_id)
        .fetch_one(&mut *transaction)
        .await?;

        let worker_id: i32 = result.try_get("id")?;
        let registration_end: NaiveDateTime = result.try_get("registration_end")?;

        transaction.commit().await?;

        Ok((worker_id, registration_end))
    }

    pub async fn request_work(&mut self, worker_id: i32) -> Result<(), sqlx::Error> {
        let mut transaction = self.conn.begin().await?;

        let _ = sqlx::query(
            r#"
                INSERT INTO async_job_assignment (worker_id, job_id)
                SELECT $1, id
                FROM scheduler_job_que s
                WHERE NOT EXISTS (
                    SELECT 1 FROM async_job_assignment a
                    WHERE a.job_id = s.id
                )
                ;
            "#,
        )
        .bind(worker_id)
        .execute(&mut *transaction)
        .await?;

        transaction.commit().await?;

        Ok(())
    }

    pub async fn fetch_work(&mut self, worker_id: i32, job_types: &HashMap<String, JobType>) -> Result<Vec<Job>, sqlx::Error> {
        let max_fetch = 1000;
        let mut jobs: Vec<Job> = Vec::new();
        let mut transaction = self.conn.begin().await.unwrap();
        let _create_tmp = sqlx::query(
            r#"
            CREATE TEMPORARY TABLE foo (job_id bigint, module text, parameters text, runtime timestamp, priority integer);
            "#)
            .execute(&mut *transaction).await?;

        let _collect_waiting_jobs = sqlx::query(
            r#"
            INSERT INTO foo
            SELECT aja.job_id, sjq.module, sjq.parameters, sjq.runtime, sjq.priority
            FROM async_job_assignment aja
            JOIN scheduler_job_que sjq on sjq.id = aja.job_id
            WHERE (
                aja.worker_id = $1
                AND aja.loaded IS NOT TRUE
                AND sjq.runtime <= CURRENT_TIMESTAMP
            )
            LIMIT $2
            ;
            "#,
        )
        .bind(worker_id)
        .bind(max_fetch)
        .execute(&mut *transaction)
        .await?;

        let _update_job_assignment = sqlx::query(
            r#"
            UPDATE async_job_assignment
            SET loaded = TRUE
            FROM foo
            WHERE foo.job_id = async_job_assignment.job_id
            ;
            "#,
        )
        .execute(&mut *transaction)
        .await?;

        let mut select_assigned_jobs = sqlx::query(
            r#"
            SELECT job_id, module, parameters, runtime, priority
            FROM foo
            ;
            "#,
        )
        .fetch(&mut *transaction);

        loop {
            match select_assigned_jobs.try_next().await {
                Ok(r) => {
                    if let Some(row) = r {
                        match Job::from_row(&row) {
                            Ok(mut job) => {
                                if let Some(j) = job_types.get(&job.job_type.name) {
                                    job.job_type = j.clone();
                                    jobs.push(job)
                                }
                            }
                            Err(e) => crate::error!("Failed to make job from row: {}", e),
                        }
                    } else {
                        // No more rows
                        break;
                    }
                }
                Err(e) => {
                    // Error
                    crate::error!("Failed to get assigned jobs: {}", e);
                    break;
                }
            }
        }

        // Release borrow of transaction. We're done with stream.
        drop(select_assigned_jobs);

        let _drop_tmp = sqlx::query(
            r#"
            DROP TABLE foo;
            "#,
        )
        .execute(&mut *transaction)
        .await?;
        transaction.commit().await.unwrap();

        trace!("Loaded {} jobs", &jobs.len());

        Ok(jobs)
    }

    pub async fn batch_work(&mut self, job: Job, worker_id: i32) -> Result<Batch, sqlx::Error> {
        let batch: Batch = Batch::new(job.job_type.clone(), 1000);
        let uuid_str = format!("{}", &batch.id.as_simple());
        let mut transaction = self.conn.begin().await?;

        let _ = if job.job_type.is_batchable {
            sqlx::query(
                r#"
                    UPDATE async_job_assignment
                    SET batch_id = $1::UUID
                    WHERE job_id IN (
                        SELECT aja.job_id
                        FROM async_job_assignment aja
                        JOIN scheduler_job_que sjq ON sjq.id = aja.job_id
                        WHERE (
                            aja.worker_id = $2
                            AND sjq.module = $3
                        )
                        LIMIT $4
                    )
                    ;
                "#,
            )
            .bind(uuid_str)
            .bind(worker_id)
            .bind(&job.job_type.name)
            .bind(batch.size)
            .execute(&mut *transaction)
        } else {
            sqlx::query(
                r#"
                    UPDATE async_job_assignment
                    SET batch_id = $1::UUID
                    WHERE (
                        job_id = $2
                        AND worker_id = $3
                    )
                    ;
                "#,
            )
            .bind(uuid_str)
            .bind(job.id)
            .bind(worker_id)
            .execute(&mut *transaction)
        }
        .await?;

        transaction.commit().await?;
        Ok(batch)
    }

    pub async fn cleanup_batch(&mut self, batch: Batch, success: bool) -> Result<(), sqlx::Error> {
        let uuid_str = format!("{}", &batch.id.as_simple());
        let percent_done = if success { 100 } else { 0 };
        let status = if success { "SUCCESS" } else { "FAILURE" };

        let mut transaction = self.conn.begin().await?;

        let _create_tmp = sqlx::query(
            r#"
                CREATE TEMP TABLE foo AS
                SELECT * FROM async_job_assignment
                WHERE batch_id = $1::UUID
                ;
            "#,
        )
        .bind(&uuid_str)
        .execute(&mut *transaction)
        .await?;

        // Record a status for each "sticky" job. This prevents re-execution,
        // though the job records are retained.
        if batch.job_type.is_sticky {
            let _record_status = sqlx::query(
                r#"
                    INSERT INTO scheduler_results (
                        scheduler_job_id,
                        completed,
                        status,
                        percent_done
                    )
                    SELECT job_id, now(), $1, $2 FROM foo
                    -- If the job itself, or some other process has already
                    -- inserted a result row, skip it.
                    ON CONFLICT (scheduler_job_id) DO NOTHING
                    ;
                "#,
            )
            .bind(&status)
            .bind(&percent_done)
            .execute(&mut *transaction)
            .await?;
        }

        // On completion or failure, disown the batch
        let _delete_batch_assignment = sqlx::query(
            r#"
                DELETE FROM async_job_assignment
                WHERE batch_id = $1::UUID
                ;
            "#,
        )
        .bind(&uuid_str)
        .execute(&mut *transaction)
        .await?;

        // Batches that do not succeed are just disowned rather than deleted.
        // Likewise, sticky jobs are marked as complete, but are not removed.
        //
        // Only successful, non-sticky jobs are removed.
        if !success || !batch.job_type.is_sticky || !batch.job_type.is_self_cleaning {
            let _delete_jobs = sqlx::query(
                r#"
                    DELETE FROM scheduler_job_que
                    WHERE id IN (
                        SELECT job_id FROM foo
                    )
                    ;
                "#,
            )
            .execute(&mut *transaction)
            .await?;
        }

        // Cleanup temp table
        let _delete_tmp = sqlx::query(
            r#"
                DROP TABLE foo;
            "#,
        )
        .execute(&mut *transaction)
        .await?;

        transaction.commit().await?;

        Ok(())
    }
}
